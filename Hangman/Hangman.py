"""
Abraham Selenke
"""

import random
import os
import time

#Creaates empty arrays
wordlvl1 = []
wordlvl2 = []
wordlvl3 = []

#Imports Word Bank
with open("WordBank.txt", "r") as file:
    Words = file.readlines()
 
#Cleans up array
for i in range(len(Words)):
    Words[i] = Words[i].rstrip()
    
j = 0

#Takes the word bank and separates the words into three arrays
for i in range(len(Words)-1):
    if len(Words[i]) == 0:
        j += 1
        i += 1
    if (j == 0):
        wordlvl1.append(Words[i])
    elif (j == 1):
        wordlvl2.append(Words[i])
    else:
        wordlvl3.append(Words[i])

del wordlvl2[0]
del wordlvl3[0]

#This function picks the level
def level(a):
    pickWord = []
    if (a == "1"):
        pickWord = random.choice(wordlvl1)
    elif (a == "2"):
        pickWord = random.choice(wordlvl2)
    elif (a == "3"):
        pickWord = random.choice(wordlvl3)
    else:
        print("Please enter a number between 1-3. "),
        b = input("Level: ")
        return level(b)
    return pickWord
    
#This function plays the game 
def Game(word,a):
    guess = a
        
    lettersUsed = []
    PlayerWord = []
    
    for letter in word:
        PlayerWord.append("_")
    
    word = list(word)
    
    #This while loop allows the player to pick the letter
    #and check if it is in the word
    while ((guess != 0) and ("_" in PlayerWord)):
            print("Guesses Left: ", guess)
            print(("Letters used: {}").format(",".join(lettersUsed)))
            print("")
            print(" ".join(PlayerWord))
            
            pGuess = input("Select a letter: ").lower()

            #Checks correct input
            if not pGuess.isalpha():
                print("You must guess a letter")
                guess += 1
            if len(pGuess) > 1:
                print("You can only guess one letter.")
                guess += 1
            if pGuess in lettersUsed:
                print("You already guessed that letter.")
                guess += 1
                if pGuess in word:
                    guess -= 1
                
            lettersUsed.append(pGuess)
            
            #Keeps tracks of your guesses
            if pGuess not in word:
                guess -= 1
            
            #Checks if the letter is in word
            for i in range(len(word)):
                if pGuess == word[i]:
                    PlayerWord[i] = pGuess
                    
             #Ending remarks
            if "_" not in PlayerWord:
                print("\nYou won! The word was", "".join(word))
            elif guess == 0:
                print("\nYou are dead. The word was", "".join(word))
            
            print("")
            
#This function plays the game when called and repeats until player stops
def Play():
    
    Again = True
    
    while Again:
        print("Please enter a level number between 1 and 3: "),
        A = input("Level: ")
        
        #Calls the level function which outputs the game word
        GameWord = level(A)
        
        #Determines the number of guesses based of the level picked
        #Calls the game to play with game word and number of guesses
        if A == '1':
            Game(GameWord,15)
        if A == '2':
            Game(GameWord,10)
        if A == '3':
            Game(GameWord,5)
        
        #Ask player to play again
        print("Would you like to play again?") 
        print("Enter yes or y for yes, anything else to quit")
        ans = input("Answer: ").lower()
        
        #Allows the termianl to wait before clearing the screen
        time.sleep(1)
        os.system('cls')
        
        #kills the while loop
        if ans not in ("yes", "y"):
            Again = False
        
#This Plays the game            
Play()