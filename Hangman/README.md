--
## Welcome to Hangman!

This here is a simple hangman game designed to challenge the player.
The player is given the option to pick from three levels.
Each level has about 20 words that are all about average length to solve.
The real difference from each level is the amount of guesses you are given.
Try to challenge yourself and see how many words you get right before you die!

Good luck and enjoy the game!

--
## The Rules of the Game!

When starting the game, you will be asked to pick a number between 1 and 3.
These numbers represent the different levels. The levels are described below:

1 -- Level one has 20 words and you are allowed 15 guesses.
2 -- Level two has 20 words and you are allowed 10 guesses.
3 -- Level three has 23 words and you are allowed 5 guesses.

If you do not enter a valid answer, you will be asked again.

Once the level is determined, the game will pick a word randomly.
The game will display the number of guesses remaining and the letters used.
It will also display the word you are trying to guess.
You will select one letter at a time and for every correct letter you guess,
it will fill in the blank on the word display. Once you have correctly guessed
the word or run out of guesses, then the game will end and tell you if you won
or lose. Lastly, the game will ask you if you want to play again.

It is up to you if you are willing to accept the challenge!
--
