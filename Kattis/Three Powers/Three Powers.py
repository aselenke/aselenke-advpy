# -*- coding: utf-8 -*-
"""
Three Powers 2.6

@author: asele
"""

import sys

def solve(x):
    nextoutput = 0
    power = 0
    print('{', end='')
    while x > 0:
        if x & 1 == 1:
            if power == 0:
                print(' {:d}'.format(3**nextoutput), end='')
            else:
                print(', {:d}'.format(3**nextoutput), end='')
            power += 1
        x >>= 1
        nextoutput += 1
    print(' }')
    
def test():
    pass        

if __name__ == "__main__":
    if (len(sys.argv) > 1 and sys.argv[1] == "test"):
        test()
    else:
        while True:
            npower = int(input())
            if npower == 0:
                break
            solve(npower-1)
