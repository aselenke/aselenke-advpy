# -*- coding: utf-8 -*-
"""
Statistics 1.8

@author: asele
"""

import sys

def solve(l):
    if len(l) == 2:
        return [l[1],l[1],0]
    
    minnum = min(l[1:len(l)])
    maxnum = max(l[1:len(l)])
    rangenum = maxnum - minnum  
    return [minnum, maxnum, rangenum]

def test():
    pass
    
if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        listnums = sys.stdin.readlines()
        casenum = 0
        for i in range(len(listnums)):
            listnums[i] = listnums[i].split()
            for j in range(len(listnums[i])):    
                listnums[i][j] = int(listnums[i][j])
        for i in range(len(listnums)):
            solvefin = solve(listnums[i])
            casenum += 1
            print("Case {}: {} {} {}".format(casenum,solvefin[0],solvefin[1],solvefin[2]))
            solvefin = []
            