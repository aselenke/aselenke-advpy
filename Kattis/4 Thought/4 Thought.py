# -*- coding: utf-8 -*-
"""
4 Thought 2.9
@author: asele
"""

import sys

def solve(x,numbersolving):
    operators = [' + ', ' - ', ' * ', ' // ']
    values ={}
    for a in operators:
        for b in operators:
            for c in operators:
                value_statement = "4{:s}4{:s}4{:s}4".format(a, b, c)
                value = eval(value_statement)
                values[value] = value_statement.replace('//', '/') + " = {:d}".format(value)
    for i in range(x):
            if numbersolving[i] < -60:
                print("no solution")
            elif numbersolving[i] > 256:
                print("no solution")
            elif numbersolving[i] not in values:
                print("no solution")
            else:
                print(values[numbersolving[i]])          
    return

def answer():
    pass

def test():
    assert solve(1,16) == '4 / 4 * 4 * 4 = 16', "Test 1 Failed"
    assert solve(1,268) == 'no solution', "Test 2 Failed"
    assert solve(1,24) == '4 * 4 + 4 + 4 = 24', "Test 3 Failed"
    assert solve(1,-16) == '4 - 4 * 4 - 4 = -16', "Test 4 Failed"
    print("All Test Cases Passed")
    
if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        x = int(input())
        numbersolving = []
        for i in range(x):
            numbersolving.append(int(input()))
        
        solve(x,numbersolving)
        
        