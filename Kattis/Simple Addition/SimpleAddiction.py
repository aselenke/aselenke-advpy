# -*- coding: utf-8 -*-
"""
Simple Addiction 2.2

@author: asele
"""

import sys

def solve(X,Y):
    return X + Y

def test():
    assert solve(5,5) == 10, "Test 1 has failed"
    assert solve(100,467) == 567, "Test 2 has failed"
    assert solve(25,45) == 70, "Test 3 has failed"
    assert solve(59999999999,1) == 60000000000, "Test 4 has failed"
    print("All Test Cases Passed")
    
if __name__ == "__main__":
    if (len(sys.argv) > 1 and sys.argv[1] == "test"):
        test()
    else:
        X = int(input())
        Y = int(input())
        print(solve(X,Y))
