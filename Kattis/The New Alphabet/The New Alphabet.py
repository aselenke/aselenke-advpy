# -*- coding: utf-8 -*-
"""
The New Alphabet 1.8

@author: asele
"""

import sys

def play(w):
    
    alp = {'a': '@','b': '8','c': '(','d': '|)','e': '3','f': '#','g': '6','h': '[-]','i': '|',
    'j': '_|','k': '|<','l': '1','m': '[]\/[]','n': '[]\[]','o': '0','p': '|D','q': '(,)',
    'r': '|Z','s': '$','t': '\'][\'','u': '|_|','v': '\/','w': '\/\/','x': '}{','y': '`/',
    'z': '2'}
    
    return ''.join([alp[change] if change in alp else change for change in w.lower()]) 

def test():
    test1 = 'Hello' 
    test2 = 'bye'
    test3 = 'Winner'
    
    assert play(test1) == '[-]3110', "Test 1 has failed"
    assert play(test2) == '8`/3', "Test 2 has failed"
    assert play(test3) == '\/\/|[]\[][]\[]3|Z', "Test 3 has failed"
    print("All Test Cases Passed")
    
if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        word = input()
        print(play(word))

 