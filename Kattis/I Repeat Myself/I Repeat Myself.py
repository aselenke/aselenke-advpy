# -*- coding: utf-8 -*-
"""
I Repeat Myself 2.4

@author: asele
"""

import sys

def solve():
    w = input()
    l = len(w)
    for j in range(1, l + 1):
        if w == (w[0:j] * (int(l // j) + 1))[0:l]:                
            ans = j
            break
    return ans


def test():
    test = 'hhhhhhhhh'
    test1 = 'HelloHelloHelloHello'
    test2 = 'abrahamabraham'
    test3 = 'arcdesbikare'
    
    assert solve(test) == 1, "Test 1 has failed"
    assert solve(test1) == 5, "Test 2 has failed"
    assert solve(test2) == 7, "Test 3 has failed"
    assert solve(test3) == 12, "Test 4 has failed"
    print("All Test Cases Passed")
    
if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        x = int(input())
        for i in range(x):
            print(solve())
        
