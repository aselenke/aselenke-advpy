# -*- coding: utf-8 -*-
"""
Numbers On A Tree 2.2

@author: asele
"""

import sys

def solve(p,H):
    placement = 0
    for i in p:
        placement = (2 * placement) + (i == 'L')

    d = len(p)
    belownum= (2 ** (H + 1)) - (2 ** (d + 1))
    return belownum + placement + 1

def test():
    pass
    
if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == 'test':
        test()
    else:
        Height, *path = input().split()
        Height = int(Height)
        path = ''.join(path)
        print(solve(path,H))
        